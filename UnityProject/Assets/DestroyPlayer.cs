﻿using UnityEngine;
using System.Collections;

public class DestroyPlayer : MonoBehaviour {
	// Update is called once per frame
	void Update () {
		Ray ray = new Ray (transform.position, transform.forward);
		RaycastHit hit;
		if (Physics.Raycast (ray, out hit, 100)) {
			if(hit.collider.gameObject.tag == "Player"){
				Destroy(hit.collider.gameObject);
			}
		}
	}
}
