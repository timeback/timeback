﻿using UnityEngine;
using System.Collections;

public class FadeOut : MonoBehaviour {
	MeshRenderer maskRenderer;
	float alpha = 1f;

	
	// Use this for initialization
	void Start () {
		maskRenderer = gameObject.GetComponent<MeshRenderer>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		

			//alpha = Mathf.Lerp(alpha, 0f, Time.deltaTime*5);
			alpha -= 0.03f;
			Color color = new Color (255, 255, 255, alpha);
			maskRenderer.material.color = color;

			if (alpha < 0)
					gameObject.SetActive (false);
	}

}
