﻿using UnityEngine;
using System.Collections;

public class FadeIn : MonoBehaviour {

	MeshRenderer maskRenderer;
	float alpha = 1f;
	[HideInInspector] public bool fade;

	// Use this for initialization
	void Start () {
		maskRenderer = gameObject.GetComponent<MeshRenderer>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {

	
		//if (fade) maskRenderer.material.Lerp(black, transparent, Time.deltaTime);
		if (fade) {
			//alpha = Mathf.Lerp(alpha, 0f, Time.deltaTime*5);
			alpha -= 0.05f;
			Color color = new Color(0, 0, 0, alpha);
			maskRenderer.material.color = color;

            if(alpha<0) gameObject.SetActive(false);
		}
	}
}
