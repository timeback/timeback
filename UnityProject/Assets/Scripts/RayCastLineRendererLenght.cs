﻿using UnityEngine;
using System.Collections;

public class RayCastLineRendererLenght : MonoBehaviour
{
		LineRenderer lr;
		// Use this for initialization
		void Start ()
		{
				lr = GetComponent<LineRenderer> ();
		}
	
		// Update is called once per frame
		void Update ()
		{
				Ray ray = new Ray (transform.position, transform.forward);
				RaycastHit hit;
				if (Physics.Raycast (ray, out hit, 100)) {
						lr.SetPosition (1, transform.InverseTransformPoint (hit.point));
						Debug.DrawLine(transform.position,transform.InverseTransformPoint (hit.point));
				} else
						lr.SetPosition (1, Vector3.forward * 100);
		}
}
