﻿using UnityEngine;
using System.Collections;

public class Teleport : MonoBehaviour {
	public GameObject activateObject;
	public bool onRecord;
	public bool onReverse;
	public bool destroy;
	void OnTriggerEnter (Collider other) {
		Replay replay = other.gameObject.GetComponent<Replay> ();
		//print (replay.record + " " + onRecord + " " + replay.reverse + " " + onReverse);
		if ((replay.record == onRecord) && (replay.reverse == onReverse)) {
			if (destroy)
			{
				Destroy(other.gameObject);
			}

			activateObject.SetActive(true);
			Debug.Log ("yeah");
			//print (gameObject.name);
		}
	}
}
