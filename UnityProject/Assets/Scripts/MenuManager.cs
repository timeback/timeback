﻿using UnityEngine;
using System.Collections;

public class MenuManager : MonoBehaviour {

	public void PlayAsGirl()
	{
		PlayGame();
	}

	public void PlayAsBoy()
	{
		PlayGame();
	}

	public void QuitGame()
	{
		Application.Quit();
	}

	public void PlayGame()
	{
		Application.LoadLevel("ReleaseScene");
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
