﻿using UnityEngine;
using System.Collections;

public class KillPlayer : MonoBehaviour {

	void OnCollisionStay(Collision collision) {
		if (collision.gameObject.tag == "Player") {
			Destroy(collision.gameObject);
		}
	}
}
