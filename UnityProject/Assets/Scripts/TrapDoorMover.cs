﻿using UnityEngine;
using System.Collections;

public class TrapDoorMover : MonoBehaviour {
	public bool doorOpen = false;
	Transform door;
	float timer;
	bool timerActivated;
	Vector3 nextPosition;
	public float openingTime;

	void OnEnable () {
		//OpenDoor();
		ToggleDoor();
	}


	void ToggleDoor() {
		if (doorOpen)
			StartCoroutine(CloseDoor());
		else
			StartCoroutine(OpenDoor());

	}

	IEnumerator OpenDoor() {
		nextPosition = new Vector3 (door.position.x, door.position.y - 0.3f, door.position.z);
		//door.position = Vector3.Lerp(door.position, nextPosition, Time.fixedDeltaTime);
		yield return new WaitForSeconds(openingTime*0.2f);
		nextPosition = new Vector3 (door.position.x - 2.5f, door.position.y, door.position.z);
		doorOpen = true;
	}

	IEnumerator CloseDoor () {
		nextPosition = new Vector3 (door.position.x, door.position.y - 0.3f, door.position.z);
		//door.position = Vector3.Lerp(door.position, nextPosition, Time.fixedDeltaTime);
		yield return new WaitForSeconds(openingTime*0.8f);
		nextPosition = new Vector3 (door.position.x - 2.5f, door.position.y, door.position.z);
		doorOpen = false;
	}

	// Use this for initialization
	void Awake () {
		door = transform.parent.transform;
	}
	
	// Update is called once per frame
	void Update () {
		door.position = Vector3.Lerp(door.position, nextPosition, Time.fixedDeltaTime);
	}
}
