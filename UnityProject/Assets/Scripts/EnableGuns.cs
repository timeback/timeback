﻿using UnityEngine;
using System.Collections;

public class EnableGuns : MonoBehaviour {

	public MultiGunController gunController;

	// Use this for initialization
	void Start () {
		gunController.enabled = true;
	}
}
