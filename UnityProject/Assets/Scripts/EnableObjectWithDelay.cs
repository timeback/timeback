﻿using UnityEngine;
using System.Collections;

public class EnableObjectWithDelay : MonoBehaviour
{
    public float delay;
    public GameObject obj;
    private float timePassed;
    
    // Update is called once per frame
    void Update ()
    {
        timePassed += Time.deltaTime;

        if (timePassed > delay) {
            obj.SetActive(true);
            this.enabled = false;
        }
    }
}
