﻿using UnityEngine;
using System.Collections.Generic;

public class MultiGunController : MonoBehaviour {
	List<Shoot> guns = new List<Shoot>();
	public float shootDelay=5f;
	float pastTime = 0;

	// Use this for initialization
	void Start () {
		for (int i=0; i<transform.childCount-1; i++) {
			guns.Add(transform.GetChild(i).GetComponent<Shoot>());
		}
	}
	
	// Update is called once per frame
	void Update () {
		pastTime += Time.deltaTime;

		if(pastTime > shootDelay){
			for (int i=0; i<transform.childCount-1; i++) {
				guns[i].shoot = true;
			}
			this.enabled = false;
		}
	}
}
