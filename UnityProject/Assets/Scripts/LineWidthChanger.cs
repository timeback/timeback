﻿using UnityEngine;
using System.Collections;

public class LineWidthChanger : MonoBehaviour
{
    LineRenderer lr;
    float timeSinceLastChange = 0;
    public float changeInternval = 0.2f;
    bool wide = false;

    // Use this for initialization
    void Start ()
    {
        lr = gameObject.GetComponent<LineRenderer> ();
    }
    
    // Update is called once per frame
    void Update ()
    {
        timeSinceLastChange += Time.deltaTime;

        if (timeSinceLastChange > changeInternval) {
            timeSinceLastChange = 0;

            if (wide) {
                lr.SetWidth (0.1f, 0.1f);
                wide = false;
            } else {
                lr.SetWidth (0.4f, 0.4f);
                wide = true;
            }
        }
    }
}
