﻿using UnityEngine;
using System.Collections;

public class WallRise : MonoBehaviour {
	public Vector3 targetScale;
	public float xScaleSpeed;
	public float yScaleSpeed;

	[Tooltip("So that we could put full height walls in scenes.")]
	public bool zeroHeightOnStart = false;
	[Tooltip("Because we put walls not on Y=0.")]
	public bool zeroYPos = false;

	// Use this for initialization
	void Start () {
		if (zeroHeightOnStart) {
			Vector3 currentScale = transform.localScale;
			Vector3 newScale = new Vector3(currentScale.x,0,currentScale.z);
			transform.localScale = newScale;
		}
		if (zeroYPos) {
			Vector3 curretPost = transform.position;
			transform.position = new Vector3(curretPost.x,0,curretPost.z);
		}
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		//increase y
		if (targetScale.y  - transform.localScale.y > yScaleSpeed) {
			transform.localScale += new Vector3(0, yScaleSpeed,0);
			//transform.position += new Vector3(0, yScaleSpeed/2,0);
		}
		//increase x
		if (targetScale.x  - transform.localScale.x > xScaleSpeed) {
			transform.localScale += new Vector3(xScaleSpeed, 0,0);
			transform.position += new Vector3(xScaleSpeed/2, 0,0);
		}
	}
}
