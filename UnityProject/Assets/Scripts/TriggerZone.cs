﻿using UnityEngine;
using System.Collections;


public class TriggerZone : MonoBehaviour {

	public GameObject activatedObject;
	public string triggerTag;
	public bool value;
	public float activationDelay;
	public bool singleActivation;
	public enum Options { Enter, Exit, Stay };
	[Tooltip("When will the trigger activate its object?")]
	public Options mode;
	float timer;
	bool timerActivated = false;
	public bool magneticTrigger = false;
	public float magneticMultiplier = 1f;
	public Collider magneticTarget;
	
	
	void OnTriggerEnter(Collider obj)
	{
		if (mode == Options.Enter)
		if (obj.gameObject.tag == triggerTag) {
			print ("I received a " + triggerTag + "!");
			activatedObject.SetActive(value);
			if (singleActivation) gameObject.SetActive(false);
		}
	}
	void OnTriggerStay(Collider obj)
	{
		if (mode == Options.Stay)
		if (obj.gameObject.tag == triggerTag) {
			//print ("The " + triggerTag + " is here!");
			
			//obj.gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
			



			timer += Time.deltaTime;
			if (magneticTrigger) magneticTarget = obj;
			if (timer >= activationDelay && timerActivated == false)
			{
				timerActivated = true;
				obj.rigidbody.mass = obj.rigidbody.mass*10;
				activatedObject.SetActive(value);
				if (singleActivation) gameObject.SetActive(false);
				ResetTimer();
			}
		}
	}
	
	void OnTriggerExit(Collider obj)
	{
		if (mode == Options.Exit)
		if (obj.gameObject.tag == triggerTag) {
			activatedObject.SetActive(value);
			ResetTimer();
			//print ("The " + triggerTag + " has left the zone!");
			if (singleActivation) gameObject.SetActive(false);
		}
	}
	void ResetTimer()
	{
		timer = 0f;
		timerActivated = false;
	}
	void FixedUpdate()
	{
		if (magneticTrigger && magneticTarget)
		{
			magneticTarget.rigidbody.AddForce((transform.position - magneticTarget.transform.position)*timer*magneticMultiplier);
		}
	}
	}
	