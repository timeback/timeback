﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour {

	public Vector3 cameraPosition = new Vector3(0, 0, 0);
	public static GameObject mainCamera;
	public static Vector3 targetCamera;
	public static int currentLevel = 1;
	public GameObject initialPlayer;
	public static GameObject player;
	public GameObject[] levels;


	// Use this for initialization
	void Start () {
		player = initialPlayer;
		mainCamera = gameObject;
		targetCamera = cameraPosition;
		mainCamera.transform.parent.position = targetCamera;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		//mainCamera.transform.position = targetCamera * 0.05f + mainCamera.transform.position * 0.95f;
		mainCamera.transform.parent.position = Vector3.Lerp(mainCamera.transform.parent.position, targetCamera, Time.deltaTime);
	}
}
