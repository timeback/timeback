﻿using UnityEngine;
using System.Collections;

public class PlayerControls : MonoBehaviour {
	public float velocity;
	public Quaternion cameraRotation;
	public float fineRotation = 0;
	static public float moveRotation;
	Animator anim;
	void Start () {
		anim = gameObject.GetComponent<Animator> ();
		//cameraRotation = GameManager.mainCamera.transform.localRotation;
	}
	void FixedUpdate () {
		float rot = 0;
		bool move = true;
		if (Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.D)) {
			rot = 180 + fineRotation;
		} else if (Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.A)) {
			rot = -90 + fineRotation;
		} else if (Input.GetKey(KeyCode.S) && Input.GetKey(KeyCode.D)) {
			rot = 90 + fineRotation;
		} else 	if (Input.GetKey(KeyCode.S) && Input.GetKey(KeyCode.A)) {
			rot = fineRotation;
		} else if (Input.GetKey(KeyCode.W)) {
			rot = 180 + 45 + fineRotation;
		} else if (Input.GetKey(KeyCode.S)) {
			rot = 45 + fineRotation;
		} else if (Input.GetKey(KeyCode.A)) {
			rot = -90 + 45 + fineRotation;
		} else 	if (Input.GetKey(KeyCode.D)) {
			rot = 45 + 90 + fineRotation;
		} else {
			move = false;
		}
		if (move) {
			rot = Mathf.Deg2Rad * rot;
			rigidbody.AddForce (new Vector3 (Mathf.Cos (rot) * velocity, 0, Mathf.Sin (rot) * velocity));
			moveRotation = rot;
			transform.rotation = Quaternion.Euler (new Vector3 (0,-PlayerControls.moveRotation * Mathf.Rad2Deg + 90, 0));
		}
		if (anim) anim.SetFloat("Velocity",Mathf.Abs(Vector3.Distance(rigidbody.velocity, Vector3.zero)));
	}

	void OnCollisionStay (Collision obj)
	{
		if (obj.gameObject.tag == "Box") {
			if (anim) anim.SetBool("Pushing",true);
		}
	}

	void OnCollisionExit (Collision obj)
	{
		if (obj.gameObject.tag == "Box") {
			if (anim) anim.SetBool("Pushing",false);
		}
	}
}
