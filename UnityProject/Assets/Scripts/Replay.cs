﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Replay : MonoBehaviour {
	public bool record = true;
	private List<ReplayData > replayDataList = new List<ReplayData >();
	private int recordFrame;
	public bool reverse = true;
	public int level;
	public bool playerInfo;

	public void Refresh()
	{
		List<ReplayData > replayDataList = new List<ReplayData >();
		record = true;
		reverse = true;
	}

	void FixedUpdate () {
		if (record) {
			recordPosition();
			if (level > 0 && level < GameManager.currentLevel) {
				Play();
			}
		} else {
			ChangeFrame();
		}
	}

	public void ChangeFrame()
	{
		transform.position = replayDataList[recordFrame].position;
		transform.rotation = replayDataList[recordFrame].rotation;
		if (playerInfo) {
			Animator anim = gameObject.GetComponent<Animator> ();
			if (reverse)
			{
				anim.SetFloat("Velocity",0);
				anim.SetBool("Pushing",false);
				anim.SetFloat("VelocityBack",replayDataList[recordFrame].velocity);
				anim.SetBool("PushingBack",replayDataList[recordFrame].pushing);
			} else {
				anim.SetFloat("VelocityBack",0);
				anim.SetBool("PushingBack",false);
				anim.SetFloat("Velocity",replayDataList[recordFrame].velocity);
				anim.SetBool("Pushing",replayDataList[recordFrame].pushing);
			}
		

		}
		if (reverse)
		{
			if (recordFrame > 2) {
				recordFrame -= 3;
			} else {
				reverse = false;
			    Play() ;
			}
		} else {
			if (recordFrame + 1 < replayDataList.Count) {
				recordFrame += 1;
			} else {

				gameObject.SetActive(false);
			}
		}
	}

	public void recordPosition()
	{
		ReplayData data = new ReplayData ();
		data.position = transform.position;
		data.rotation = transform.rotation;
		if (playerInfo) {
			Animator anim = gameObject.GetComponent<Animator> ();
			data.velocity = anim.GetFloat("Velocity");
			data.pushing = anim.GetBool("Pushing");
		}
		replayDataList.Add(data);
	}

	public void Play() {
		record = false;
		if (reverse)
		{
			recordFrame = replayDataList.Count - 1;
		} else{
			recordFrame = 0;
		}
	}

    public bool isPlayingBack(){
        if (!reverse && !record)
            return true;
        else
            return false;
    }
}

public class ReplayData  {
	public Vector3 position;
	public Quaternion rotation;

	public float velocity;
	public bool pushing;
}
