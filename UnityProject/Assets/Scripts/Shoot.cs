﻿using UnityEngine;
using System.Collections;

public class Shoot : MonoBehaviour {
	private Vector3 bulletHole;
	public GameObject bullet;
	public bool shoot = false;
	public float bulletSpeed = 5f;
	private float delay;
	
	void Start () {
		delay = Random.Range (100, 160);
	}
	void FixedUpdate()
	{
		delay -= 1;
		if (delay < 1) {
			bulletHole = transform.GetChild (0).position;
			Vector3 shootDirection = Vector3.Normalize(bulletHole - transform.position)*bulletSpeed;
			GameObject newBullet = (GameObject)Instantiate (bullet,bulletHole,new Quaternion());
			StartMoving bulletMovement = newBullet.GetComponent<StartMoving> ();
			bulletMovement.speedVector = shootDirection;
			Destroy (gameObject);
		}

	}
    void OnDrawGizmos(){
        Gizmos.color = Color.red;
        Vector3 direction = transform.TransformDirection(-Vector3.left) * 20;
        Gizmos.DrawRay(transform.position, direction);
    }
}
