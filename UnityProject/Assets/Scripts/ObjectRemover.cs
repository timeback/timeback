﻿using UnityEngine;
using System.Collections;

public class ObjectRemover : MonoBehaviour {
	public GameObject[] gameObjects;
	// Use this for initialization
	void Start () {
		for (int i = 0; i < gameObjects.Length; i++)
		{
			gameObjects[i].SetActive(false);
		}
	}

}
