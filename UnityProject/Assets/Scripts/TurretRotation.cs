﻿using UnityEngine;
using System.Collections.Generic;

public class TurretRotation : MonoBehaviour {
	public float yRotation = 10f;

	// Update is called once per frame
	void Update () {
		transform.Rotate (Vector3.up, yRotation*Time.deltaTime);
	}
}
