﻿using UnityEngine;
using System.Collections;

public class DropZone : MonoBehaviour {
	
	public GameObject targetObject;
	public bool value;
	public float activationDelay;
	float timer;
	bool activated;
	

	void OnTriggerEnter(Collider obj)
	{
		if (obj.gameObject.tag == "Box") {
			print ("I received a box!");
		}
	}
	void OnTriggerStay(Collider obj)
	{
		if (obj.gameObject.tag == "Box") {
			print ("The box is here!");
			timer += Time.deltaTime;
			if (timer >= activationDelay && activated == false)
			{
				activated = true;
				ResetTimer();
				targetObject.SetActive(value);
			}
		}
	}

	void OnTriggerExit(Collider obj)
	{
		if (obj.gameObject.tag == "Box") {
			ResetTimer();
			print ("The box has left the zone!");
		}
	}
	void ResetTimer()
	{
		timer = 0f;
		activated = false;
	}
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
