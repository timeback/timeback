﻿using UnityEngine;
using System.Collections;

public class PlayerSplitter : MonoBehaviour {
	//Transform self = GameObject;
	PlayerControls controls;

	public void Split () {
		GameObject g = Instantiate(gameObject) as GameObject;
		GameManager.player = g;
		g.transform.GetComponent<Replay>().Refresh();
		//collider.enabled = false;
		rigidbody.useGravity = false;
		controls.enabled = false;
		gameObject.tag = "Untagged";
		transform.GetComponent<Replay>().Play();
	}

	// Use this for initialization
	void Start () {
		controls = transform.GetComponent<PlayerControls>();
	
	}
	
	// Update is called once per frame
	void Update () {

	}
}
