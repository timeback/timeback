﻿using UnityEngine;
using System.Collections;

public class TurnOffRotationOnReplay : MonoBehaviour {
	
	// Update is called once per frame
	void Update () {
		Replay replay = GetComponent<Replay> ();
		if (!replay.record) {
			TurretRotation rotation = GetComponent<TurretRotation>();
			rotation.enabled = false;
		}
	}
}
