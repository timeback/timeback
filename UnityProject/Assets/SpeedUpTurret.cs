﻿using UnityEngine;
using System.Collections;

public class SpeedUpTurret : MonoBehaviour {
	public TurretRotation turretRot;
	public float multiplier = 2;
	// Use this for initialization
	void Start () {
		turretRot.yRotation *=  multiplier;
	}
}
