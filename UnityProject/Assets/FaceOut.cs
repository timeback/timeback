﻿using UnityEngine;
using System.Collections;

public class FaceOut : MonoBehaviour {
	public GameObject musicObject;


	void Start () {


	}
	
	// Update is called once per frame
	void Update () {
		AudioSource musicSource = musicObject.GetComponent<AudioSource> ();
		if (musicSource.volume > 0f)
		{
			musicSource.volume -= 0.005f;
		}
	}
}
