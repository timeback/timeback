﻿using UnityEngine;
using System.Collections;

public class BoxShadow : MonoBehaviour {
	public GameObject box;


	void Update () {
		if (box != null) {

						transform.position = new Vector3 (box.transform.position.x, transform.position.y, box.transform.position.z);
				
			} else {
					Destroy (gameObject);
			}
	}
}
